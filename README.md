### Step To Run Program :

```
1. Buat Database dengan nama "majoo" pada localhost
2. jalankan migration dengan perintah " migrate -path db/migrations -database "mysql://root@(localhost:3306)/majoo -verbose up " atau " make migrate-up "
3. Jalankan Perintah " Make run "
4. Buka Browser dengan alamat url " http://localhost:1323/docs/index.html "
5. Dianjurkan untuk membuat merchent terlebih dahulu kerena table lainnya menggunakan foreign key id pada table merchent
```

### Install Packages

```
go mod init
go mod tidy
```

### Start Server

```
make run
```

### API Docs

`http://localhost:1323/docs/index.html`

### If Swagger is error or cannot run swag init

```
export PATH=$(go env GOPATH)/bin:$PATH
```

### Syntac to create or make Migrations

```
migrate create -ext sql -dir db/migrations create_table_users
```

### Syntac for Migrations

```
migrate -path db/migrations -database "mysql://root@(localhost:3306)/majoo" -verbose up
```

### Echo

```
https://echo.labstack.com/
```

### Grom

```
orm: https://gorm.io/
```

## Swagger

```
echo-swagger: https://github.com/swaggo/echo-swagger
```

### Migration

```
https://dev.to/techschoolguru/how-to-write-run-database-migration-in-golang-5h6g
https://github.com/golang-migrate/migrate/tree/master/cmd/migrate
https://scoop.sh/
```
