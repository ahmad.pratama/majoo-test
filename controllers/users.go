package controllers

import (
	"majoo/core"
	"majoo/helper"
	"majoo/models"
	"strconv"

	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/thedevsaddam/govalidator"
	"golang.org/x/crypto/bcrypt"
)

type (
	UserRequest struct {
		Username string `json:"username"`
		Name string `json:"name"`
		Alamat string `json:"alamat"`
		NoTelp string `json:"no_telp"`
		JenisKelamin string `json:"jk"`
		Role string `json:"role"`
		Email string `json:"email"`
		Password string `json:"password"`
		MerchentId string `json:"merchent_id"`
	}

	LoginUser struct {
		Username string `json:"username"`
		Email string `json:"email"`
		Password string `json:"password"`
	}
)


// UserLogin login user
// @Summary Login user
// @Description Login user
// @Tags Users
// @ID user-login
// @Accept json
// @Produce application/json
// @Param RequestBody body LoginUser true "JSON Request Body"
// @Success 200 {object} string
// @Router /api/v1/user/login [post]
func UserLogin(c echo.Context) error {
	defer c.Request().Body.Close()

	requestBody := new(LoginUser)
	if err := c.Bind(requestBody); err != nil {
		return helper.Response(http.StatusBadRequest, err, "Binding request error")
	}

	response := map[string]interface{}{}

	type (
		UserEmailFilter struct {
			Email string `condition:"WHERE" json:"email"`
		}

		UsernameFilter struct {
			Username string `condition:"WHERE" json:"username"`
		}
	)

	authenticated := false

	userEmail := models.User{}
	if requestBody.Email != "" {
		UserEmailFilter := UserEmailFilter{
			Email: requestBody.Email,
		}
		if userEmail.Find(&UserEmailFilter); userEmail.ID != 0 {
			err := bcrypt.CompareHashAndPassword([]byte(userEmail.Password), []byte(requestBody.Password))
			if err == nil {
				authenticated = true
			} else {
				userEmail = models.User{}
			}
		}
	}

	userUsername := models.User{}
	if requestBody.Username != "" {
		usernameFilter := UsernameFilter{
			Username: requestBody.Username,
		}
		if userUsername.Find(&usernameFilter); userUsername.ID != 0 {
			err := bcrypt.CompareHashAndPassword([]byte(userUsername.Password), []byte(requestBody.Password))
			if err == nil {
				authenticated = true
			} else {
				userUsername = models.User{}
			}
		}
	}

	if !authenticated {
		return helper.Response(http.StatusUnauthorized, "Wrong username/email or password", "Wrong username/email or password")
	}

	userData := models.User{}
	if userUsername.ID != 0 {
		userData = userUsername
	}
	if userEmail.ID != 0 {
		userData = userEmail
	}

	token, err := helper.CreateJwtToken(userData.ID, userData.Name)
	if err != nil {
		return helper.Response(http.StatusInternalServerError, err, "Error creating token")
	}

	response["token"] = token
	return c.JSON(http.StatusOK, response)
}

// UserCreate Create user
// @Summary Create user
// @Description Create user
// @Tags Users
// @ID users-user-create
// @Accept mpfd
// @Produce plain
// @Param username formData string true "Username"
// @Param name formData string true "Name"
// @Param alamat formData string true "Alamat"
// @Param no_telp formData string true "No Telfon"
// @Param jk formData string true "Jenis Kelamin" Enums(laki-laki, perempuan)
// @Param role formData string true "Role User" Enums(admin, user)
// @Param email formData string true "Email"
// @Param password formData string true "Password"
// @Param merchent_id formData string true "Id Merchent"
// @Success 200 {object} models.User{}
// @Router /api/v1/user/create [post]
func UserCreate(c echo.Context) error {
	defer c.Request().Body.Close()

	payloadRules := govalidator.MapData{
		"username": []string{"required"},
		"name": []string{"required"},
		"alamat":   []string{},
		"no_telp": []string{},
		"jk": []string{},
		"role": []string{"required"},
		"email":   []string{"required"},
		"password":   []string{"required"},
		"merchent_id": []string{"required"},
	}

	validate := helper.ValidateRequestFormData(c, payloadRules)
	if validate != nil {
		return helper.Response(http.StatusUnprocessableEntity, validate, "Validation error")
	}

	tx := core.App.DB.Begin()

	type (
		UserEmailFilter struct {
			Email string `condition:"WHERE" json:"email"`
		}

		UsernameFilter struct {
			Username string `condition:"WHERE" json:"username"`
		}
	)


	checkExistingUser := models.User{}
	userEmailFilter := UserEmailFilter{
		Email: c.FormValue("email"),
	}
	if checkExistingUser.Find(&userEmailFilter); checkExistingUser.ID != 0 {
		tx.Rollback()
		return helper.Response(http.StatusBadRequest, "Error", "Validation user error: email already used")
	}

	checkExistingUser = models.User{}
	usernameFilter := UsernameFilter{
		Username: c.FormValue("username"),
	}
	if checkExistingUser.Find(&usernameFilter); checkExistingUser.ID != 0 {
		tx.Rollback()
		return helper.Response(http.StatusBadRequest, "Error", "Validation user error: username already used")
	}
	password := c.FormValue("password")
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		return helper.Response(http.StatusBadRequest, err, "Hash password failed")
	}
	hashString := string(hash)

	Merchent_id, _ := strconv.Atoi(c.FormValue("merchent_id"))

	users := models.User{
		Username: c.FormValue("username"), 
		Name: c.FormValue("name"), 
		Alamat: c.FormValue("alamat"),
		NoTelp: c.FormValue("no_telp"),
		JenisKelamin: c.FormValue("jk"),
		Role: c.FormValue("role"),
		Email: c.FormValue("email"),
		Password: hashString,
		MerchentId: Merchent_id,
	}

	if err := tx.Create(&users).Error; err != nil {
		tx.Rollback()
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while saving the Admin data")
	}

	tx.Commit()

	response := helper.HttpResponse{
		Status: http.StatusOK,
		Message: "success",
		Data: map[string]interface{}{
			"username": c.FormValue("username"),
			"name": c.FormValue("name"),
			"alamat": c.FormValue("alamat"),
			"jk": c.FormValue("jk"),
			"role": c.FormValue("role"),
			"email": c.FormValue("email"),
			"password": c.FormValue("password"),
			"hash password": hashString,
			"merchent_id": Merchent_id,
		},
	}


	return c.JSON(http.StatusCreated, response)
}

// UserList Get list of user
// @Summary Get list of user
// @Description Get list of user
// @Tags Users
// @ID users_jk-users_jk-list
// @Accept mpfd
// @Produce plain
// @Param page query integer false "page number" default(1)
// @Param pageSize query integer false "number of user in single page" default(10)
// @param jk query string true "jenis kelamin" Enums(Laki-laki, Perempuan)
// @Success 200 {object} []models.User
// @Router /api/v1/user/list-user-jk [get]
func UserListJK(c echo.Context) error {
	defer c.Request().Body.Close()

	users := models.User{}
	rows, err := strconv.Atoi(c.QueryParam("pageSize"))
	page, err := strconv.Atoi(c.QueryParam("page"))
	orderby := "created_at"
	sort := "DESC"

	JenisKelamin := c.QueryParam("jk")

	type Filter struct {
		JK string `condition:"LIKE" json:"jk"`
	}
	filter := Filter{
		JK: JenisKelamin,
	}
	result, err := users.PagedFilterSearch(page, rows, orderby, sort, &filter)

	if err != nil {
		return helper.Response(http.StatusInternalServerError, err, "query result error")
	}

	response := helper.HttpResponseData{
		Status: http.StatusOK,
		Message: "Success",
		Data: result.Data,
	}

	return c.JSON(http.StatusOK, response)
}


// UserList Get list of user
// @Summary Get list of user
// @Description Get list of user
// @Tags Users
// @ID users_nama-users_nama-list
// @Accept mpfd
// @Produce plain
// @Param page query integer false "page number" default(1)
// @Param pageSize query integer false "number of user in single page" default(10)
// @param nama query string true "nama user"
// @Success 200 {object} []models.User
// @Router /api/v1/user/list-user-nama [get]
func UserListNama(c echo.Context) error {
	defer c.Request().Body.Close()

	users := models.User{}
	rows, err := strconv.Atoi(c.QueryParam("pageSize"))
	page, err := strconv.Atoi(c.QueryParam("page"))
	orderby := "created_at"
	sort := "DESC"

	Nama := c.FormValue("nama")

	type Filter struct {
		Name string `condition:"LIKE" json:"name"`
	}
	filter := Filter{
		Name: Nama,
	}
	result, err := users.PagedFilterSearch(page, rows, orderby, sort, &filter)

	if err != nil {
		return helper.Response(http.StatusInternalServerError, err, "query result error")
	}

	response := helper.HttpResponseData{
		Status: http.StatusOK,
		Message: "Success",
		Data: result.Data,
	}

	return c.JSON(http.StatusOK, response)
}


// UserList Get list of user
// @Summary Get list of user
// @Description Get list of user
// @Tags Users
// @ID users-user-list
// @Accept mpfd
// @Produce plain
// @Param page query integer false "page number" default(1)
// @Param pageSize query integer false "number of user in single page" default(10)
// @Success 200 {object} []models.User
// @Router /api/v1/user/list-user [get]
func UserList(c echo.Context) error {
	defer c.Request().Body.Close()

	users := models.User{}
	rows, err := strconv.Atoi(c.QueryParam("pageSize"))
	page, err := strconv.Atoi(c.QueryParam("page"))
	orderby := "created_at"
	sort := "DESC"

	var filter struct{}
	result, err := users.PagedFilterSearch(page, rows, orderby, sort, &filter)

	if err != nil {
		return helper.Response(http.StatusInternalServerError, err, "query result error")
	}

	response := helper.HttpResponseData{
		Status: http.StatusOK,
		Message: "Success",
		Data: result.Data,
	}

	return c.JSON(http.StatusOK, response)
}


// UserDelete Delete user
// @Summary Delete user
// @Description Delete user
// @Tags Users
// @ID users-user-delete
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Success 200 {object} models.User{}
// @Router /api/v1/user/delete/{id} [delete]
func UserDelete(c echo.Context) error {
	defer c.Request().Body.Close()

	User_id, _ := strconv.Atoi(c.Param("id"))
	users := models.User{}
	if err := users.FindbyID(User_id); err != nil {
		return helper.Response(http.StatusNotFound, err, "User not found")
	}

	if err := users.Delete(); err != nil {
		return helper.Response(http.StatusUnprocessableEntity, err, "Delete User failed")
	}

	return c.JSON(http.StatusOK, users)
}

// UserDetail Detail user
// @Summary Detail user
// @Description Detail user
// @Tags Users
// @ID users-user-detail
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Success 200 {object} models.User{}
// @Router /api/v1/user/detail/{id} [get]
func UserDetail(c echo.Context) error {
	defer c.Request().Body.Close()

	User_id, _ := strconv.Atoi(c.Param("id"))
	users := models.User{}
	if err := users.FindbyID(User_id); err != nil {
		return helper.Response(http.StatusNotFound, err, "User not found")
	}

	return c.JSON(http.StatusOK, users)
}

// UserUpdate Update user
// @Summary Update user
// @Description Update user
// @Tags Users
// @ID users-user-update
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Param RequestBody body UserRequest true "JSON Request Body"
// @Success 200 {object} string
// @Router /api/v1/user/update/{id} [patch]
func UserUpdate(c echo.Context) error {
	defer c.Request().Body.Close()

	user_ID, _ := strconv.Atoi(c.Param("id"))
	user := models.User{}
	if err := user.FindbyID(user_ID); err != nil {
		return helper.Response(http.StatusNotFound, err, "User not found")
	}

	payloadRules := govalidator.MapData{
		"username": []string{},
		"name": []string{},
		"alamat":   []string{},
		"no_telp": []string{},
		"jk": []string{},
		"email":   []string{},
		"password":   []string{},
		"merchent_id": []string{},
	}
	requestBody := UserRequest{}
	validate := helper.ValidateRequestPayload(c, payloadRules, &requestBody)
	if validate != nil {
		return helper.Response(http.StatusUnprocessableEntity, validate, "Validation error")
	}

	password := requestBody.Password
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		return helper.Response(http.StatusBadRequest, err, "Hash password failed")
	}
	hashString := string(hash)

	Merchent_id, _ := strconv.Atoi(requestBody.MerchentId)

	user.Username = requestBody.Username
	user.Name = requestBody.Name
	user.Alamat = requestBody.Alamat
	user.NoTelp = requestBody.NoTelp
	user.JenisKelamin = requestBody.JenisKelamin
	user.Email = requestBody.Email
	user.Password = hashString
	user.MerchentId = Merchent_id

	if err := user.Save(); err != nil {
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while updating Magazine data")
	}

	return c.JSON(http.StatusOK, user)
}