package controllers

import (
	"majoo/core"
	"majoo/helper"
	"majoo/models"
	"strconv"

	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/thedevsaddam/govalidator"
)

type (
	OutletRequest struct {
		Name string `json:"name"`
		Alamat string `json:"alamat"`
		MerchentId string `json:"merchent_id"`
	}
)

// OutletCreate Create outlet
// @Summary Create outlet
// @Description Create outlet
// @Tags Outlets
// @ID outlets-outlet-create
// @Accept mpfd
// @Produce plain
// @Param name formData string true "Name"
// @Param alamat formData string true "Alamat"
// @Param merchent_id formData string true "Id Merchent"
// @Success 200 {object} models.Outlet{}
// @Router /api/v1/outlet/create [post]
func OutletCreate(c echo.Context) error {
	defer c.Request().Body.Close()

	payloadRules := govalidator.MapData{
		"name": []string{"required"},
		"alamat":   []string{},
		"merchent_id": []string{"required"},
	}

	validate := helper.ValidateRequestFormData(c, payloadRules)
	if validate != nil {
		return helper.Response(http.StatusUnprocessableEntity, validate, "Validation error")
	}

	Merchent_id, _ := strconv.Atoi(c.FormValue("merchent_id"))
	tx := core.App.DB.Begin()

	outlets := models.Outlet{ 
		Name: c.FormValue("name"), 
		Alamat: c.FormValue("alamat"),
		MerchentId: Merchent_id,
	}

	if err := tx.Create(&outlets).Error; err != nil {
		tx.Rollback()
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while saving the Outlet data")
	}

	tx.Commit()

	response := helper.HttpResponse{
		Status: http.StatusOK,
		Message: "Berhasil Membuat Outlet",
		Data: map[string]interface{}{
			"name": c.FormValue("name"),
			"alamat": c.FormValue("alamat"),
			"merchent_id": Merchent_id,
		},
	}


	return c.JSON(http.StatusCreated, response)
}

// OutletListNama Get list of outlet
// @Summary Get list of outlet
// @Description Get list of outlet
// @Tags Outlets
// @ID outlets_name-outlet_name-list
// @Accept mpfd
// @Produce plain
// @Param page query integer false "page number" default(1)
// @Param pageSize query integer false "number of outlet in single page" default(10)
// @param nama query string true "nama ooutlet"
// @Success 200 {object} []models.Outlet
// @Router /api/v1/outlet/list-outlet-nama [get]
func OutletListNama(c echo.Context) error {
	defer c.Request().Body.Close()

	outlets := models.Outlet{}
	rows, err := strconv.Atoi(c.QueryParam("pageSize"))
	page, err := strconv.Atoi(c.QueryParam("page"))
	orderby := "created_at"
	sort := "DESC"

	Nama := c.FormValue("nama")

	type Filter struct {
		Name string `condition:"LIKE" json:"name"`
	}
	filter := Filter{
		Name: Nama,
	}
	result, err := outlets.PagedFilterSearch(page, rows, orderby, sort, &filter)

	if err != nil {
		return helper.Response(http.StatusInternalServerError, err, "query result error")
	}

	response := helper.HttpResponseData{
		Status: http.StatusOK,
		Message: "Success",
		Data: result.Data,
	}

	return c.JSON(http.StatusOK, response)
}

// OutletList Get list of outlet
// @Summary Get list of outlet
// @Description Get list of outlet
// @Tags Outlets
// @ID outlets-outlet-list
// @Accept mpfd
// @Produce plain
// @Param page query integer false "page number" default(1)
// @Param pageSize query integer false "number of outlet in single page" default(10)
// @Success 200 {object} []models.Outlet
// @Router /api/v1/outlet/list-outlet [get]
func OutletList(c echo.Context) error {
	defer c.Request().Body.Close()

	outlets := models.Outlet{}
	rows, err := strconv.Atoi(c.QueryParam("pageSize"))
	page, err := strconv.Atoi(c.QueryParam("page"))
	orderby := "created_at"
	sort := "DESC"

	var filter struct{}
	result, err := outlets.PagedFilterSearch(page, rows, orderby, sort, &filter)

	if err != nil {
		return helper.Response(http.StatusInternalServerError, err, "query result error")
	}

	response := helper.HttpResponseData{
		Status: http.StatusOK,
		Message: "Success",
		Data: result.Data,
	}

	return c.JSON(http.StatusOK, response)
}

// OutletDetail Detail outlet
// @Summary Detail outlet
// @Description Detail outlet
// @Tags Outlets
// @ID outlets-outlet-detail
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Success 200 {object} models.Outlet{}
// @Router /api/v1/outlet/detail/{id} [get]
func OutletDetail(c echo.Context) error {
	defer c.Request().Body.Close()

	Outlet_id, _ := strconv.Atoi(c.Param("id"))
	outlets := models.Outlet{}
	if err := outlets.FindbyID(Outlet_id); err != nil {
		return helper.Response(http.StatusNotFound, err, "Outlet not found")
	}

	return c.JSON(http.StatusOK, outlets)
}

// OutletDelete Delete outlet
// @Summary Delete outlet
// @Description Delete outlet
// @Tags Outlets
// @ID outlets-outlet-delete
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Success 200 {object} models.Outlet{}
// @Router /api/v1/outlet/delete/{id} [delete]
func OutletDelete(c echo.Context) error {
	defer c.Request().Body.Close()

	Outlet_id, _ := strconv.Atoi(c.Param("id"))
	outlets := models.Outlet{}
	if err := outlets.FindbyID(Outlet_id); err != nil {
		return helper.Response(http.StatusNotFound, err, "Outlet not found")
	}

	if err := outlets.Delete(); err != nil {
		return helper.Response(http.StatusUnprocessableEntity, err, "Delete Outlet failed")
	}

	return c.JSON(http.StatusOK, outlets)
}

// OutletUpdate Update outlet
// @Summary Update outlet
// @Description Update outlet
// @Tags Outlets
// @ID outlets-outlet-update
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Param RequestBody body OutletRequest true "JSON Request Body"
// @Success 200 {object} string
// @Router /api/v1/outlet/update/{id} [patch]
func OutletUpdate(c echo.Context) error {
	defer c.Request().Body.Close()

	Outlet_ID, _ := strconv.Atoi(c.Param("id"))
	outlets := models.Outlet{}
	if err := outlets.FindbyID(Outlet_ID); err != nil {
		return helper.Response(http.StatusNotFound, err, "Outlet not found")
	}

	payloadRules := govalidator.MapData{
		"name": []string{"required"},
		"alamat":   []string{},
		"merchent_id": []string{},
	}
	requestBody := OutletRequest{}
	validate := helper.ValidateRequestPayload(c, payloadRules, &requestBody)
	if validate != nil {
		return helper.Response(http.StatusUnprocessableEntity, validate, "Validation error")
	}

	Merchent_id, _ := strconv.Atoi(requestBody.MerchentId)

	outlets.Name = requestBody.Name
	outlets.Alamat = requestBody.Alamat
	outlets.MerchentId = Merchent_id

	if err := outlets.Save(); err != nil {
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while updating Merchent data")
	}

	return c.JSON(http.StatusOK, outlets)
}