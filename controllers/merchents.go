package controllers

import (
	"majoo/core"
	"majoo/helper"
	"majoo/models"
	"strconv"

	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/thedevsaddam/govalidator"
)

type (
	MerchentRequest struct {
		Name string `json:"name"`
		Alamat string `json:"alamat"`
		Jenis string `json:"jenis"`
	}
)

// MerchentCreate Create merchent
// @Summary Create merchent
// @Description Create merchent
// @Tags Merchents
// @ID merchents-merchent-create
// @Accept mpfd
// @Produce plain
// @Param name formData string true "Name"
// @Param alamat formData string true "Alamat"
// @Param jenis formData string true "Jenis Merchent" Enums(gold, silver, platinum, bronze)
// @Success 200 {object} models.Merchent{}
// @Router /api/v1/merchent/create [post]
func MerchentCreate(c echo.Context) error {
	defer c.Request().Body.Close()

	payloadRules := govalidator.MapData{
		"name": []string{"required"},
		"alamat":   []string{"required"},
		"jenis": []string{"required"},
	}

	validate := helper.ValidateRequestFormData(c, payloadRules)
	if validate != nil {
		return helper.Response(http.StatusUnprocessableEntity, validate, "Validation error")
	}

	tx := core.App.DB.Begin()

	merchents := models.Merchent{ 
		Name: c.FormValue("name"), 
		Alamat: c.FormValue("alamat"),
		Jenis: c.FormValue("jenis"),
	}

	if err := tx.Create(&merchents).Error; err != nil {
		tx.Rollback()
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while saving the Merchent data")
	}

	tx.Commit()

	response := helper.HttpResponse{
		Status: http.StatusOK,
		Message: "Berhasil Membuat Merchent",
		Data: map[string]interface{}{
			"name": c.FormValue("name"),
			"alamat": c.FormValue("alamat"),
			"jenis": c.FormValue("jenis"),
		},
	}


	return c.JSON(http.StatusCreated, response)
}

// MerchentListJenis Get list of merchent
// @Summary Get list of merchent
// @Description Get list of merchent
// @Tags Merchents
// @ID merchent_jenis-merchent_jenis-list
// @Accept mpfd
// @Produce plain
// @Param page query integer false "page number" default(1)
// @Param pageSize query integer false "number of merchent in single page" default(10)
// @param jenis query string true "jenis merchent" Enums(gold, silver, platinum, bronze)
// @Success 200 {object} []models.Merchent
// @Router /api/v1/merchent/list-merchent-jenis [get]
func MerchentListJenis(c echo.Context) error {
	defer c.Request().Body.Close()

	merchents := models.Merchent{}
	rows, err := strconv.Atoi(c.QueryParam("pageSize"))
	page, err := strconv.Atoi(c.QueryParam("page"))
	orderby := "created_at"
	sort := "DESC"

	Jenis := c.QueryParam("jenis")

	type Filter struct {
		Jenis string `condition:"LIKE" json:"jenis"`
	}
	filter := Filter{
		Jenis: Jenis,
	}
	result, err := merchents.PagedFilterSearch(page, rows, orderby, sort, &filter)

	if err != nil {
		return helper.Response(http.StatusInternalServerError, err, "query result error")
	}

	response := helper.HttpResponseData{
		Status: http.StatusOK,
		Message: "Success",
		Data: result.Data,
	}

	return c.JSON(http.StatusOK, response)
}

// MerchentListNama Get list of merchent
// @Summary Get list of merchent
// @Description Get list of merchent
// @Tags Merchents
// @ID merchents_name-merchent_name-list
// @Accept mpfd
// @Produce plain
// @Param page query integer false "page number" default(1)
// @Param pageSize query integer false "number of merchent in single page" default(10)
// @param nama query string true "nama merchent"
// @Success 200 {object} []models.Merchent
// @Router /api/v1/merchent/list-merchent-nama [get]
func MerchentListNama(c echo.Context) error {
	defer c.Request().Body.Close()

	merchents := models.Merchent{}
	rows, err := strconv.Atoi(c.QueryParam("pageSize"))
	page, err := strconv.Atoi(c.QueryParam("page"))
	orderby := "created_at"
	sort := "DESC"

	Nama := c.FormValue("nama")

	type Filter struct {
		Name string `condition:"LIKE" json:"name"`
	}
	filter := Filter{
		Name: Nama,
	}
	result, err := merchents.PagedFilterSearch(page, rows, orderby, sort, &filter)

	if err != nil {
		return helper.Response(http.StatusInternalServerError, err, "query result error")
	}

	response := helper.HttpResponseData{
		Status: http.StatusOK,
		Message: "Success",
		Data: result.Data,
	}

	return c.JSON(http.StatusOK, response)
}

// MerchentList Get list of merchent
// @Summary Get list of merchent
// @Description Get list of merchent
// @Tags Merchents
// @ID merchents-merchent-list
// @Accept mpfd
// @Produce plain
// @Param page query integer false "page number" default(1)
// @Param pageSize query integer false "number of merchent in single page" default(10)
// @Success 200 {object} []models.Merchent
// @Router /api/v1/merchent/list-merchent [get]
func MerchentList(c echo.Context) error {
	defer c.Request().Body.Close()

	merchents := models.Merchent{}
	rows, err := strconv.Atoi(c.QueryParam("pageSize"))
	page, err := strconv.Atoi(c.QueryParam("page"))
	orderby := "created_at"
	sort := "DESC"

	var filter struct{}
	result, err := merchents.PagedFilterSearch(page, rows, orderby, sort, &filter)

	if err != nil {
		return helper.Response(http.StatusInternalServerError, err, "query result error")
	}

	response := helper.HttpResponseData{
		Status: http.StatusOK,
		Message: "Success",
		Data: result.Data,
	}

	return c.JSON(http.StatusOK, response)
}

// MerchentDetail Detail merchent
// @Summary Detail merchent
// @Description Detail merchent
// @Tags Merchents
// @ID merchents-merchent-detail
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Success 200 {object} models.Merchent{}
// @Router /api/v1/merchent/detail/{id} [get]
func MerchentDetail(c echo.Context) error {
	defer c.Request().Body.Close()

	Merchent_id, _ := strconv.Atoi(c.Param("id"))
	merchents := models.Merchent{}
	if err := merchents.FindbyID(Merchent_id); err != nil {
		return helper.Response(http.StatusNotFound, err, "Merchent not found")
	}

	return c.JSON(http.StatusOK, merchents)
}

// MerchentDelete Delete merchent
// @Summary Delete merchent
// @Description Delete merchent
// @Tags Merchents
// @ID merchents-merchent-delete
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Success 200 {object} models.Merchent{}
// @Router /api/v1/merchent/delete/{id} [delete]
func MerchentDelete(c echo.Context) error {
	defer c.Request().Body.Close()

	Merchent_id, _ := strconv.Atoi(c.Param("id"))
	merchents := models.Merchent{}
	if err := merchents.FindbyID(Merchent_id); err != nil {
		return helper.Response(http.StatusNotFound, err, "Merchent not found")
	}

	if err := merchents.Delete(); err != nil {
		return helper.Response(http.StatusUnprocessableEntity, err, "Delete Merchent failed")
	}

	return c.JSON(http.StatusOK, merchents)
}

// MerchentUpdate Update merchent
// @Summary Update merchent
// @Description Update merchent
// @Tags Merchents
// @ID merchents-merchent-update
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Param RequestBody body MerchentRequest true "JSON Request Body"
// @Success 200 {object} string
// @Router /api/v1/merchent/update/{id} [patch]
func MerchentUpdate(c echo.Context) error {
	defer c.Request().Body.Close()

	Merchent_ID, _ := strconv.Atoi(c.Param("id"))
	merchents := models.Merchent{}
	if err := merchents.FindbyID(Merchent_ID); err != nil {
		return helper.Response(http.StatusNotFound, err, "User not found")
	}

	payloadRules := govalidator.MapData{
		"name": []string{"required"},
		"alamat":   []string{},
		"jenis": []string{},
	}
	requestBody := MerchentRequest{}
	validate := helper.ValidateRequestPayload(c, payloadRules, &requestBody)
	if validate != nil {
		return helper.Response(http.StatusUnprocessableEntity, validate, "Validation error")
	}

	merchents.Name = requestBody.Name
	merchents.Alamat = requestBody.Alamat
	merchents.Jenis = requestBody.Jenis

	if err := merchents.Save(); err != nil {
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while updating Merchent data")
	}

	return c.JSON(http.StatusOK, merchents)
}