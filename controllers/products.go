package controllers

import (
	"fmt"
	"io"
	"majoo/core"
	"majoo/helper"
	"majoo/models"
	"os"
	"path/filepath"
	"strconv"

	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/thedevsaddam/govalidator"
)

type (
	ProductRequest struct {
		Name string `json:"name"`
		SKU string `json:"sku"`
		Description string `json:"description"`
		Code string `json:"code"`
		MerchentId string `json:"merchentId"`
		Image string `json:"image"`
	}
)

// ProductCreate Create product
// @Security ApiKeyAuth
// @Summary Create product
// @Description Create product
// @Tags Products
// @ID products-product-create
// @Accept mpfd
// @Produce plain
// @Param name formData string true "Name"
// @Param sku formData string true "SKU"
// @Param description formData string true "Deskripsi"
// @Param code formData string true "Code"
// @Param merchent_id formData string true "Id Merchent"
// @Success 200 {object} string
// @Router /api/v1/product/create [post]
func ProductCreate(c echo.Context) error {
	defer c.Request().Body.Close()

	payloadRules := govalidator.MapData{
		"name": []string{"required"},
		"sku": []string{},
		"description": []string{},
		"code": []string{"required"},
		"merchent_id": []string{"required"},
		"images": []string{},
	}

	products := models.Product{}

	validate := helper.ValidateRequestFormData(c, payloadRules)
	if validate != nil {
		return helper.Response(http.StatusUnprocessableEntity, validate, "Validation error")
	}


	tx := core.App.DB.Begin()

	Merchent_id, _ := strconv.Atoi(c.FormValue("merchent_id"))

	products.Name = c.FormValue("name")
	products.SKU = c.FormValue("sku")
	products.Description = c.FormValue("description")
	products.Code = c.FormValue("code")
	products.MerchentId = Merchent_id

	if err := tx.Create(&products).Error; err != nil {
		tx.Rollback()
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while saving the Product data")
	}

	tx.Commit()

	response := helper.HttpResponse{
		Status: http.StatusOK,
		Message: "Berhasil Membuat Product",
		Data: map[string]interface{}{
			"name": c.FormValue("name"),
			"sku": c.FormValue("sku"),
			"description": c.FormValue("description"),
			"code": c.FormValue("code"),
			"merchent_id": Merchent_id,
		},
	}


	return c.JSON(http.StatusCreated, response)
}


// ProductListNama Get list of product
// @Security ApiKeyAuth
// @Summary Get list of product
// @Description Get list of product
// @Tags Products
// @ID product_name-product_name-list
// @Accept mpfd
// @Produce plain
// @Param page query integer false "page number" default(1)
// @Param pageSize query integer false "number of product in single page" default(10)
// @param nama query string true "nama product"
// @Success 200 {object} []models.Product
// @Router /api/v1/product/list-product-nama [get]
func ProductListNama(c echo.Context) error {
	defer c.Request().Body.Close()

	products := models.Product{}
	rows, err := strconv.Atoi(c.QueryParam("pageSize"))
	page, err := strconv.Atoi(c.QueryParam("page"))
	orderby := "created_at"
	sort := "DESC"

	Nama := c.FormValue("nama")

	type Filter struct {
		Name string `condition:"LIKE" json:"name"`
	}
	filter := Filter{
		Name: Nama,
	}
	result, err := products.PagedFilterSearch(page, rows, orderby, sort, &filter)

	if err != nil {
		return helper.Response(http.StatusInternalServerError, err, "query result error")
	}

	response := helper.HttpResponseData{
		Status: http.StatusOK,
		Message: "Success",
		Data: result.Data,
	}

	return c.JSON(http.StatusOK, response)
}

// ProductList Get list of product
// @Security ApiKeyAuth
// @Summary Get list of product
// @Description Get list of product
// @Tags Products
// @ID products-product-list
// @Accept mpfd
// @Produce plain
// @Param page query integer false "page number" default(1)
// @Param pageSize query integer false "number of product in single page" default(10)
// @Success 200 {object} string
// @Router /api/v1/product/list-product [get]
func ProductList(c echo.Context) error {
	defer c.Request().Body.Close()

	products := models.Product{}
	rows, err := strconv.Atoi(c.QueryParam("pageSize"))
	page, err := strconv.Atoi(c.QueryParam("page"))
	orderby := "created_at"
	sort := "DESC"

	var filter struct{}
	result, err := products.PagedFilterSearch(page, rows, orderby, sort, &filter)

	if err != nil {
		return helper.Response(http.StatusInternalServerError, err, "query result error")
	}

	response := helper.HttpResponseData{
		Status: http.StatusOK,
		Message: "Success",
		Data: result.Data,
	}

	return c.JSON(http.StatusOK, response)
}

// ProductDetail Detail product
// @Security ApiKeyAuth
// @Summary Detail product
// @Description Detail product
// @Tags Products
// @ID products-product-detail
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Success 200 {object} models.Product{}
// @Router /api/v1/product/detail/{id} [get]
func ProductDetail(c echo.Context) error {
	defer c.Request().Body.Close()

	Merchent_id, _ := strconv.Atoi(c.Param("id"))
	products := models.Product{}
	if err := products.FindbyID(Merchent_id); err != nil {
		return helper.Response(http.StatusNotFound, err, "Product not found")
	}

	return c.JSON(http.StatusOK, products)
}

// ProductDelete Delete product
// @Security ApiKeyAuth
// @Summary Delete product
// @Description Delete product
// @Tags Products
// @ID products-product-delete
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Success 200 {object} models.Product{}
// @Router /api/v1/product/delete/{id} [delete]
func ProductDelete(c echo.Context) error {
	defer c.Request().Body.Close()

	Product_id, _ := strconv.Atoi(c.Param("id"))
	products := models.Product{}
	if err := products.FindbyID(Product_id); err != nil {
		return helper.Response(http.StatusNotFound, err, "Product not found")
	}

	if err := products.Delete(); err != nil {
		return helper.Response(http.StatusUnprocessableEntity, err, "Delete Product failed")
	}

	return c.JSON(http.StatusOK, products)
}

// ProductUpdate Update product
// @Security ApiKeyAuth
// @Summary Update product
// @Description Update product
// @Tags Products
// @ID products-product-update
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Param RequestBody body ProductRequest true "JSON Request Body"
// @Success 200 {object} string
// @Router /api/v1/product/update/{id} [patch]
func ProductUpdate(c echo.Context) error {
	defer c.Request().Body.Close()

	Product_ID, _ := strconv.Atoi(c.Param("id"))
	products := models.Product{}
	if err := products.FindbyID(Product_ID); err != nil {
		return helper.Response(http.StatusNotFound, err, "Product not found")
	}

	payloadRules := govalidator.MapData{
		"name": []string{"required"},
		"sku": []string{},
		"description": []string{},
		"code": []string{},
		"merchent_id": []string{},
	}
	requestBody := ProductRequest{}
	validate := helper.ValidateRequestPayload(c, payloadRules, &requestBody)
	if validate != nil {
		return helper.Response(http.StatusUnprocessableEntity, validate, "Validation error")
	}

	Merchent_id, _ := strconv.Atoi(requestBody.MerchentId)

	products.Name = requestBody.Name
	products.SKU = requestBody.SKU
	products.Description = requestBody.Description
	products.Code = requestBody.Code
	products.MerchentId = Merchent_id


	if err := products.Save(); err != nil {
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while updating Product data")
	}

	return c.JSON(http.StatusOK, products)
}

// ProductUpload Upload product
// @Security ApiKeyAuth
// @Summary Upload product
// @Description Upload product
// @Tags Products
// @ID products-product-upload
// @Accept mpfd
// @Produce plain
// @Param id path int true "ID"
// @Param product_image formData file true "Product Image"
// @Success 200 {object} string
// @Router /api/v1/product/upload/{id} [patch]
func ProductUpload(c echo.Context) error {
	defer c.Request().Body.Close()

	Product_ID, _ := strconv.Atoi(c.Param("id"))
	products := models.Product{}
	if err := products.FindbyID(Product_ID); err != nil {
		return helper.Response(http.StatusNotFound, err, "Product not found")
	}

	tx := core.App.DB.Begin()
	
	if _, err := c.FormFile("product_image"); err != nil {
		tx.Rollback()
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while uploading Product Image #1")	
	}

	file, err := c.FormFile("product_image")
	if err != nil {
		tx.Rollback()
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while uploading Product Image #2")
	}

	src, err := file.Open()
	if err != nil {
		tx.Rollback()
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while uploading Product Image #3")
	}
	defer src.Close()

	filename := fmt.Sprintf("%d%s", Product_ID, filepath.Ext(file.Filename))
	fileLocation := filepath.Join("assets", filename)

	fmt.Println(fileLocation)
	dst, err := os.Create(fileLocation)
	if err != nil {
		tx.Rollback()
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while uploading Product Image #4")
	}
	defer dst.Close()

	if _, err = io.Copy(dst, src); err != nil {
		tx.Rollback()
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while uploading Product Image #5")
	}

	products.Image = fileLocation
	
	if err := products.Save(); err != nil {
		return helper.Response(http.StatusUnprocessableEntity, err, "Error while updating Merchent data")
	}

	return c.JSON(http.StatusOK, products)
}