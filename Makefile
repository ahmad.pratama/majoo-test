BIN_NAME=majoo
#!make
include .env
export $(shell sed 's/=.*//' .env)

exports:
	@printenv | grep MYAPP

run: exports generate-docs
	@go run main.go

test-env:
	$(eval include .env.test)
	$(eval export $(shell sed 's/=.*//' .env.test))

test: drop-db-test migrate-up-test clear-test-cache test-env exports unit-test

test-no-reset: test-env exports unit-test

clear-test-cache: 
	@go clean -testcache

generate-docs:
	@echo "Updating API documentation..."
	@swag init

migrate-up:
	@migrate -path db/migrations -database "mysql://${MYAPP_DB_USER}:${MYAPP_DB_PASSWORD}@(${MYAPP_DB_HOST}:${MYAPP_DB_PORT})/${MYAPP_DB_NAME}" -verbose up

migrate-down:
	@migrate -path db/migrations -database "mysql://${MYAPP_DB_USER}:${MYAPP_DB_PASSWORD}@(${MYAPP_DB_HOST}:${MYAPP_DB_PORT})/${MYAPP_DB_NAME}" -verbose down
