package main

import (
	"majoo/controllers"
	"majoo/core"
	_ "majoo/docs"
	"majoo/middlewares"

	"os"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger"
)

// @title CMS Eventori API
// @version 1.0
// @description CMS Eventori API V1
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	defer func(App *core.Application) {
		err := App.Close()
		if err != nil {

		}
	}(core.App)

	e := echo.New()
	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "${status} ${time_rfc3339} [${method}] ${uri} ; remote_ip=${remote_ip} ; error=${error}\n",
	}))
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"*"},
		AllowHeaders: []string{"*"},
	}))
	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Request().Header.Set("Cache-Control", "max-age:3600, public")
			c.Response().Header().Set("X-Robots-Tag", "noindex")
			return next(c)
		}
	})

	e.Pre(middleware.Rewrite(map[string]string{
		"/api/*": "/$1",
	}))

	e.GET("/docs/*", echoSwagger.WrapHandler)

	api := e.Group("/v1")
	{
		users := api.Group("/user")
		users.POST("/create", controllers.UserCreate)
		users.GET("/list-user-jk", controllers.UserListJK)
		users.GET("/list-user-nama", controllers.UserListNama)
		users.GET("/list-user", controllers.UserList)
		users.GET("/detail/:id", controllers.UserDetail)
		users.DELETE("/delete/:id", controllers.UserDelete)
		users.PATCH("/update/:id", controllers.UserUpdate)
		users.POST("/login", controllers.UserLogin)

		merchents := api.Group("/merchent")
		merchents.POST("/create", controllers.MerchentCreate)
		merchents.GET("/list-merchent-jenis", controllers.MerchentListJenis)
		merchents.GET("/list-merchent-nama", controllers.MerchentListNama)
		merchents.GET("/list-merchent", controllers.MerchentList)
		merchents.GET("/detail/:id", controllers.MerchentDetail)
		merchents.DELETE("/delete/:id", controllers.MerchentDelete)
		merchents.PATCH("/update/:id", controllers.MerchentUpdate)

		products := api.Group("/product")
		{
		middlewares.AuthenticationMiddleware(products)
		products.POST("/create", controllers.ProductCreate)
		products.GET("/list-product-nama", controllers.ProductListNama)
		products.GET("/list-product", controllers.ProductList)
		products.GET("/detail/:id", controllers.ProductDetail)
		products.DELETE("/delete/:id", controllers.ProductDelete)
		products.PATCH("/update/:id", controllers.ProductUpdate)
		products.PATCH("/upload/:id", controllers.ProductUpload)
	}

		outlets := api.Group("/outlet")
		outlets.POST("/create", controllers.OutletCreate)
		outlets.GET("/list-outlet-nama", controllers.OutletListNama)
		outlets.GET("/list-outlet", controllers.OutletList)
		outlets.GET("/detail/:id", controllers.OutletDetail)
		outlets.DELETE("/delete/:id", controllers.OutletDelete)
		outlets.PATCH("/update/:id", controllers.OutletUpdate)
	}

	e.Logger.Fatal(e.Start(":" + core.App.Port))
	os.Exit(0)

}