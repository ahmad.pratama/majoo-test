package middlewares

import (
	"majoo/core"
	"net/http"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func AuthenticationMiddleware(g *echo.Group) {
	g.Use(setBearerRule)
	g.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningMethod: "HS256",
		SigningKey:    []byte(core.App.Config.JWT_SECRET),
	}))
	g.Use(validateJWTClient)
}

func setBearerRule(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		tokenString := c.Request().Header.Get("Authorization")
		if strings.HasPrefix(tokenString, "Bearer") == false {
			c.Request().Header.Set("Authorization", "Bearer "+tokenString)
		}
		return next(c)
	}
}

func validateJWTClient(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		user := c.Get("user")
		token := user.(*jwt.Token)
		if claims, ok := token.Claims.(jwt.MapClaims); ok {
			c.Set("user_data", claims)
			return next(c)
		}
		return echo.NewHTTPError(http.StatusUnauthorized, "Unathorized")
	}
}
