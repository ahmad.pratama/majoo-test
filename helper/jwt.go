package helper

import (
	"majoo/core"
	"strconv"

	"github.com/dgrijalva/jwt-go"
)

type (
	JWTclaims struct {
		ID        int `json:"id"`
		Name 	  string `json:"name"`
		jwt.StandardClaims
	}
)

func CreateJwtToken(userID int, name string) (string, error) {
	claim := JWTclaims{
		userID,
		name,
		jwt.StandardClaims{
			Id: strconv.Itoa(userID),
		},
	}

	rawToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	token, err := rawToken.SignedString([]byte(core.App.Config.JWT_SECRET))
	if err != nil {
		return "", err
	}

	return token, nil
}