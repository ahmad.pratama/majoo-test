CREATE TABLE suppliers (
    id INT(11) NOT NULL,
    name VARCHAR(100),
    alamat TEXT,
    no_telp varchar(50),
    nama_product varchar(255),
    merchent_id INT(11),
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY `fk_merchent_supplier` (`merchent_id`),
    CONSTRAINT `fk_merchent_supplier` FOREIGN KEY (`merchent_id`) REFERENCES `merchents` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
);

ALTER TABLE suppliers
ADD PRIMARY KEY (id);

ALTER TABLE suppliers
MODIFY id int(11) NOT NULL AUTO_INCREMENT;

