CREATE TABLE product_outlets (
    id INT(11) NOT NULL,
    harga VARCHAR(100),
    quantity VARCHAR(10),
    outlet_id int(11),
    product_id int(11),
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY `fk_outlet_id` (`outlet_id`),
    CONSTRAINT `fk_outlet_id` FOREIGN KEY (`outlet_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    KEY `fk_product_id` (`product_id`),
    CONSTRAINT `fk_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
);

ALTER TABLE product_outlets
ADD PRIMARY KEY (id);

ALTER TABLE product_outlets
MODIFY id int(11) NOT NULL AUTO_INCREMENT;