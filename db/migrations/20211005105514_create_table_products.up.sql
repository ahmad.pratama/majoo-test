CREATE TABLE products (
    id INT(11) NOT NULL,
    name VARCHAR(100),
    sku VARCHAR(100),
    description TEXT,
    code VARCHAR(100),
    image TEXT,
    merchent_id INT(11),
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY `fk_merchent_product` (`merchent_id`),
    CONSTRAINT `fk_merchent_product` FOREIGN KEY (`merchent_id`) REFERENCES `merchents` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
);

ALTER TABLE products
ADD PRIMARY KEY (id);

ALTER TABLE products
MODIFY id int(11) NOT NULL AUTO_INCREMENT;

