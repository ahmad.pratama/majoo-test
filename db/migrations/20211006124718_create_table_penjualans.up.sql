CREATE TABLE penjualans (
    id INT(11) NOT NULL,
    tanggal date,
    amount DECIMAL(65),
    costumer_id int(11),
    product_id int(11),
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY `fk_product_costumer` (`costumer_id`),
    CONSTRAINT `fk_product_costumer` FOREIGN KEY (`costumer_id`) REFERENCES `costumers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    KEY `fk_product_jual` (`product_id`),
    CONSTRAINT `fk_product_jual` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
);

ALTER TABLE penjualans
ADD PRIMARY KEY (id);

ALTER TABLE penjualans
MODIFY id int(11) NOT NULL AUTO_INCREMENT;