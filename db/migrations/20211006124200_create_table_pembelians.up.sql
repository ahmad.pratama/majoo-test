CREATE TABLE pembelians (
    id INT(11) NOT NULL,
    tanggal date,
    restock varchar(100),
    supplier_id int(11),
    product_id int(11),
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY `fk_product_supplier` (`supplier_id`),
    CONSTRAINT `fk_product_supplier` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    KEY `fk_product_beli` (`product_id`),
    CONSTRAINT `fk_product_beli` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
);

ALTER TABLE pembelians
ADD PRIMARY KEY (id);

ALTER TABLE pembelians
MODIFY id int(11) NOT NULL AUTO_INCREMENT;