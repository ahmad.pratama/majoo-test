CREATE TABLE outlets (
    id INT(11) NOT NULL,
    name VARCHAR(100),
    alamat text,
    merchent_id INT(11),
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY `fk_merchent_outlet` (`merchent_id`),
    CONSTRAINT `fk_merchent_outlet` FOREIGN KEY (`merchent_id`) REFERENCES `merchents` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
);

ALTER TABLE outlets
ADD PRIMARY KEY (id);

ALTER TABLE outlets
MODIFY id int(11) NOT NULL AUTO_INCREMENT;

