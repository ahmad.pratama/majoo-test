CREATE TABLE merchents (
    id INT(11) NOT NULL,
    name VARCHAR(255),
    alamat text,
    jenis enum('gold','silver','platinum','bronze'),
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

ALTER TABLE merchents
ADD PRIMARY KEY (id);

ALTER TABLE merchents
MODIFY id int(11) NOT NULL AUTO_INCREMENT;