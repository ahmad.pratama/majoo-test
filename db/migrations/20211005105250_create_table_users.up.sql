CREATE TABLE users (
    id INT(11) NOT NULL,
    username VARCHAR(150),
    name VARCHAR(255),
    alamat text,
    no_telp VARCHAR(30),
    jk enum('laki-laki','perempuan'),
    role enum('admin','user'),
    email VARCHAR(150),
    password VARCHAR(100),
    merchent_id INT(11),
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY `fk_merchent_user` (`merchent_id`),
    CONSTRAINT `fk_merchent_user` FOREIGN KEY (`merchent_id`) REFERENCES `merchents` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
);

ALTER TABLE users
ADD PRIMARY KEY (id);

ALTER TABLE users
MODIFY id int(11) NOT NULL AUTO_INCREMENT;