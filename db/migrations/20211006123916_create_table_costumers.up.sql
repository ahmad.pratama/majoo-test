CREATE TABLE costumers (
    id INT(11) NOT NULL,
    name VARCHAR(100),
    alamat TEXT,
    no_telp varchar(50),
    type enum('member','non-membet'),
    outlet_id INT(11),
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY `fk_outlet_costumer` (`outlet_id`),
    CONSTRAINT `fk_outlet_costumer` FOREIGN KEY (`outlet_id`) REFERENCES `outlets` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
);

ALTER TABLE costumers
ADD PRIMARY KEY (id);

ALTER TABLE costumers
MODIFY id int(11) NOT NULL AUTO_INCREMENT;

