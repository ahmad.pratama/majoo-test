package models

import (
	"majoo/core"
)

type (
	Product struct {
		core.Model
		Name string `json:"name" gorm:"column:name"`
		SKU string `json:"sku" gorm:"column:sku"`
		Image string `json:"image" gorm:"column:image"`
		Description string `json:"description" gorm:"column:description"`
		Code string `json:"code" gorm:"column:code"`
		MerchentId int `json:"merchent_id" gorm:"column:merchent_id"`
	}
)

func (Product) TableName() string {
	return "products"
}

func (p *Product) Create() error {
	err := core.Create(&p)
	return err
}

func (p *Product) Save() error {
	err := core.Save(&p)
	return err
}

func (p *Product) Delete() error {
	err := core.Delete(&p)
	return err
}

func (p *Product) FindbyID(id int) error {
	err := core.FindbyID(&p, id)
	return err
}

func (b *Product) PagedFilterSearch(page int, rows int, orderby string, sort string, filter interface{}) (result core.PagedFindResult, err error) {
	Question := []Product{}
	orders := []string{orderby}
	sorts := []string{sort}
	result, err = core.PagedFindFilter(&Question, page, rows, orders, sorts, filter, []string{})

	return result, err
}

func (p *Product) Find(filter interface{}) error {
	err := core.Find(&p, filter)
	return err
}