package models

import (
	"majoo/core"
)

type (
	User struct {
		core.Model
		Username string `json:"username" gorm:"column:username"`
		Name string `json:"name" gorm:"column:name"`
		Alamat string `json:"alamat" gorm:"column:alamat"`
		NoTelp string `json:"no_telp" gorm:"column:no_telp"`
		JenisKelamin string `json:"jk" gorm:"column:jk"`
		Role string `json:"role" gorm:"column:role"`
		Email string `json:"email" gorm:"column:email"`
		Password string `json:"password" gorm:"column:password"`
		MerchentId int `json:"merchent_id" gorm:"column:merchent_id"`
	}
)

func (User) TableName() string {
	return "users"
}

func (p *User) Create() error {
	err := core.Create(&p)
	return err
}

func (p *User) Save() error {
	err := core.Save(&p)
	return err
}

func (p *User) Delete() error {
	err := core.Delete(&p)
	return err
}

func (p *User) FindbyID(id int) error {
	err := core.FindbyID(&p, id)
	return err
}

func (b *User) PagedFilterSearch(page int, rows int, orderby string, sort string, filter interface{}) (result core.PagedFindResult, err error) {
	Question := []User{}
	orders := []string{orderby}
	sorts := []string{sort}
	result, err = core.PagedFindFilter(&Question, page, rows, orders, sorts, filter, []string{})

	return result, err
}

func (p *User) Find(filter interface{}) error {
	err := core.Find(&p, filter)
	return err
}