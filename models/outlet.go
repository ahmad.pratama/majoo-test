package models

import (
	"majoo/core"
)

type (
	Outlet struct {
		core.Model
		Name string `json:"name" gorm:"column:name"`
		Alamat string `json:"alamat" gorm:"column:alamat"`
		MerchentId int `json:"merchent_id" gorm:"column:merchent_id"`
	}
)

func (Outlet) TableName() string {
	return "outlets"
}

func (p *Outlet) Create() error {
	err := core.Create(&p)
	return err
}

func (p *Outlet) Save() error {
	err := core.Save(&p)
	return err
}

func (p *Outlet) Delete() error {
	err := core.Delete(&p)
	return err
}

func (p *Outlet) FindbyID(id int) error {
	err := core.FindbyID(&p, id)
	return err
}

func (b *Outlet) PagedFilterSearch(page int, rows int, orderby string, sort string, filter interface{}) (result core.PagedFindResult, err error) {
	Question := []Outlet{}
	orders := []string{orderby}
	sorts := []string{sort}
	result, err = core.PagedFindFilter(&Question, page, rows, orders, sorts, filter, []string{})

	return result, err
}

func (p *Outlet) Find(filter interface{}) error {
	err := core.Find(&p, filter)
	return err
}