package models

import (
	"majoo/core"
)

type (
	Merchent struct {
		core.Model
		Name string `json:"name" gorm:"column:name"`
		Alamat string `json:"alamat" gorm:"column:alamat"`
		Jenis string `json:"jenis" gorm:"column:jenis"`
	}
)

func (Merchent) TableName() string {
	return "merchents"
}

func (p *Merchent) Create() error {
	err := core.Create(&p)
	return err
}

func (p *Merchent) Save() error {
	err := core.Save(&p)
	return err
}

func (p *Merchent) Delete() error {
	err := core.Delete(&p)
	return err
}

func (p *Merchent) FindbyID(id int) error {
	err := core.FindbyID(&p, id)
	return err
}

func (b *Merchent) PagedFilterSearch(page int, rows int, orderby string, sort string, filter interface{}) (result core.PagedFindResult, err error) {
	Question := []Merchent{}
	orders := []string{orderby}
	sorts := []string{sort}
	result, err = core.PagedFindFilter(&Question, page, rows, orders, sorts, filter, []string{})

	return result, err
}

func (p *Merchent) Find(filter interface{}) error {
	err := core.Find(&p, filter)
	return err
}